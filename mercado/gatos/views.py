from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .models import informacion

from .grafos import *
from secrets import randbelow
#import requests, json, urllib

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import gatosSerializer

@api_view(['GET', 'POST'])
def informacion_list(request):
    """
    List all code Books, or create a new Book.
    """
    if request.method == 'GET':
        mercados = informacion.objects.all()

        for m in mercados:
        	m.contenido = str(m.contenido).replace("\r\n",",")
        	#print("")
        	#print("contenido: ", m.contenido)
        	#print("")



        serializer = gatosSerializer(mercados, many=True)

        s_d = serializer.data
        print("")
        print("type",type(s_d), " s_d: ",s_d)
        print("")


        return Response(serializer.data)

    elif request.method == 'POST':

        contenido = request.data["contenido"]

        renglon = ""
        array_renglones = []

        for c in contenido:
            if c != ",":
                renglon += c
            else:
                array_renglones.append(renglon)
                renglon = ""
        array_renglones.append(renglon)
        print("array_renglones: ",array_renglones)

        encabezado = []
        pescados_por_tienda = []
        carreteras = []
        pescados_por_tienda_l = []
        carreteras_l = []

        if len(array_renglones) >0 :
            encabezado=array_renglones[0].split()
            print("")
            print("encabezado: ",encabezado, " N. tiendas,  N. tipos de pescado, N. de carreteras")
            pescados_por_tienda = array_renglones[1: int(encabezado[0])+1]
            print("")
            #print("pescados_por_tienda ", pescados_por_tienda)
            carreteras = array_renglones[int(encabezado[0])+1:len(array_renglones)]
            #print("carreteras ", carreteras)
            #print("")

        for p_t in pescados_por_tienda:
            pescados_por_tienda_l.append(p_t.split())
        print("pescados_por_tienda_l ", pescados_por_tienda_l)

        for p_t in carreteras:
            carreteras_l.append(p_t.split())
        print("carreteras_l ", carreteras_l)
        print("")


        g = Grafica()

        l = list(range(1, int(encabezado[0])+1))
        print("l: //// ",l)

        for v in l:
            g.agregarVertice(v)


        for i in carreteras_l:
            g.agregarArista(int(i[0]),int(i[1]))
            #print("i[0],i[1]" , i[0],i[1])



        for v in g.vertices:
            print(v, g.vertices[v].vecinos)
            print("")

        v1 = 1
        vf = int(encabezado[0])

        rutas = []
        pescados = []
        demoras = []
        sumas = []
        contador =0

        def tiempo(a,b):
            a= int(a)
            b= int(b)
            for carretera in carreteras_l:
                if int(carretera[0]) == a and int(carretera[1]) == b:
                    return int(carretera[2])
                if int(carretera[1]) == a and int(carretera[0]) == b:
                    return int(carretera[2])
            return 0


        for n in range (0, 20):
            v1 = 1
            ruta = []
            pescado = []
            demora = []
            suma = []

            for x in range(1, len(pescados_por_tienda_l[v1-1])):
                if not int(pescados_por_tienda_l[v1-1][x]) in pescado:
                    pescado.append(int(pescados_por_tienda_l[v1-1][x]))

            while v1 != vf :
                vecinos = g.vertices[v1].vecinos
                va=v1
                vx  = randbelow(len(vecinos))
                v1 = vecinos[vx]
                ruta.append(v1)
                for x in range(1, len(pescados_por_tienda_l[v1-1])):
                    if not int(pescados_por_tienda_l[v1-1][x]) in pescado:
                        pescado.append(int(pescados_por_tienda_l[v1-1][x]))

                demora.append(tiempo(va,v1))


            if not ruta in rutas :
                contador += 1
                rutas.append(ruta)
                pescados.append(pescado)
                demoras.append(demora)
                suma.append(sum(demora))
                sumas.append(sum(demora))
                print("r: ",n,"_",contador,"_",ruta,pescado,demora,suma)

        n_rutas= len(rutas)

        print("sumas",sumas)

        indices = []
        totales = []

        lista_pescados= list(range(1,int(encabezado[1])+1))



        def unique(list1):
            # insert the list to the set
            list_set = set(list1)
            # convert the set to the list
            return list(list_set)


        t_minimo=100000

        # LAS combinaciones sin repeticion se guardan en indices
        #

        for n in range(0,n_rutas):
            for x in range(0,n_rutas):
                indice  = []
                x1=""
                x2=""
                x4=""
                x5=""
                x6=""
                if x<n:
                    indice = [x ,n]
                    x1= pescados[x]
                    x2= pescados[n]
                else:
                    indice = [n ,x]
                    x1= pescados[x]
                    x2= pescados[n]
                x4 = sumas[x]
                x5 = sumas[n]
                if x4>x5:
                    x6=x4
                else:
                    x6=x5
                if not indice in indices:
                    indices.append(indice)
                    x3=unique(x1+x2)
                    if x3 == lista_pescados:
                        #print("x3",x3, "tiempo:",x6)
                        if x6<t_minimo:
                        	t_minimo =x6

        print("tiempo_minimo:",t_minimo)


        #print("indices: ", indices)






        contenido = str(contenido).replace(",","\r\n")
        request.data["contenido"] = contenido
        request.data["tiempo"] = str(t_minimo)

        serializer = gatosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




def index(request):
    return HttpResponse('Hello, World!')
