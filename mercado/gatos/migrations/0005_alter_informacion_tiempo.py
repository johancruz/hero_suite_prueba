# Generated by Django 3.2.3 on 2021-05-14 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gatos', '0004_alter_informacion_tiempo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='informacion',
            name='tiempo',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
