from secrets import randbelow


class Vertice:

    def __init__(self, i):
        self.id = i
        self.visitado = False
        self.nivel = -1
        self.vecinos = []

    def agregarVecino(self, v):
        if not v in self.vecinos:
           self.vecinos.append(v)


class Grafica:

    def __init__(self):
        self.vertices = {}

    def agregarVertice(self, v):
        if not v in self.vertices:
           self.vertices[v] = Vertice(v)

    def agregarArista(self, a, b):
        if a in self.vertices and b in self.vertices:
           self.vertices[a].agregarVecino(b)
           self.vertices[b].agregarVecino(a)


# si desea probar este codigo por aparte
# basta con descomentar las siguientes lineas


"""
def main():

    g = Grafica()

    l = [0,1,2,3,4,5,6,]

    for v in l:
        g.agregarVertice(v)

    l = [2,0  , 0,6  , 6,3, 0,5, 6,5, 0,1 , 6,4 ,1 ,4 ]

    for i in range(0, len(l)-1,2):
        g.agregarArista(l[i],l[i+1])

    for v in g.vertices:
        print(v, g.vertices[v].vecinos)

    v1 = 0
    vf = 4

    rutas = []
    contador =0

    for n in range (0, 10000):
        v1 = 0
        ruta = []
        while v1 != vf :
            vecinos = g.vertices[v1].vecinos
            vx  = randbelow(len(vecinos))
            v1 = vecinos[vx]
            ruta.append(v1)
        if not ruta in rutas :
            contador += 1
            rutas.append(ruta)
            print("r: ",n,"_",contador,"_",ruta)

    #print(rutas)



main()

"""
